// creating an object
const userDetail: userDetailsFormat = {

  name: "Abhi",
  age: 10,
  login: () => {
    console.log("Login");
  },
  logout: () => {
    console.log("LogOut");
  },
};
userDetail.login();
userDetail.logout();

//Updating properties of object

userDetail["name"] = "Ram";
userDetail.age = 19;

console.log(userDetail.name);

// dynamically access/update the properties of an object
let property: keyof userDetailsFormat = "name";

userDetail[property] = "Ganesh";
property = "age";

console.log(userDetail[property]);

// Class
class user {
  name: string;
  age: number;
  constructor(nameInput: string, ageInput: number) {
    this.name = nameInput;
    this.age = ageInput;
  }
  login(): void {
    console.log("Login");
  }
  logout(): void {
    console.log("Logout");
  }
}

const user1 = new user("Ramu", 23);
user1.logout();

// Method Chaining

class Customer {
  name: string;
  age: number;
  creditPoints: number;
  constructor(nameInput: string, ageInput: number) {
    this.name = nameInput;
    this.age = ageInput;
    this.creditPoints = 0;
  }
  login(): any {
    console.log(`${this.name} Logged in`);
    return this;
  }
  logout(): any {
    console.log(`${this.name} Logged out`);
    return this;
  }
  updateCreditPoints(): any {
    this.creditPoints++;
    console.log(`Credit Points : ${this.creditPoints}`);
    return this;
  }
}

const Customer1 = new Customer("Raman",34);
const Customer2 = new Customer("Rajeev",50);
Customer1.login().updateCreditPoints().updateCreditPoints().logout();


let Customers = [Customer1,Customer2]

// inheritance
class Admin extends Customer{
    deleteUser(customer:any){
      Customers = Customers.filter(cus => customer.name !== cus.name)
    }
}
const admin1 = new Admin("admin1",25);
admin1.deleteUser(Customer1);

